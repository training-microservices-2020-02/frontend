package id.artivisi.training.microservice.frontend.service;

import id.artivisi.training.microservice.frontend.dto.MateriPelatihan;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Map;

@FeignClient(name="registrasi", fallback = RegistrasiServiceFallback.class)
public interface RegistrasiService {

    @GetMapping("/api/materi/")
    Iterable<MateriPelatihan> daftarMateri();

    @GetMapping("/api/backend/")
    Map<String, Object> backendInfo();
}
