package id.artivisi.training.microservice.frontend.controller;

import id.artivisi.training.microservice.frontend.service.RegistrasiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class BackendInfoController {

    @Autowired private RegistrasiService registrasiService;

    @GetMapping("/backend")
    public ModelMap backend() {
        return new ModelMap()
                .addAttribute("backend", registrasiService.backendInfo());
    }
}
