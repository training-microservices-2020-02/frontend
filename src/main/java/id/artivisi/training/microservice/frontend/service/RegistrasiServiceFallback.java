package id.artivisi.training.microservice.frontend.service;

import id.artivisi.training.microservice.frontend.dto.MateriPelatihan;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@Component @Slf4j
public class RegistrasiServiceFallback implements RegistrasiService {
    @Override
    public Iterable<MateriPelatihan> daftarMateri() {
        log.info("Menjalankan fallback method untuk daftar materi");
        return new ArrayList<>();
    }

    @Override
    public Map<String, Object> backendInfo() {
        log.info("Menjalankan fallback method untuk backend info");
        Map<String, Object> fallbackInfo = new HashMap<>();
        fallbackInfo.put("host", "offline");
        fallbackInfo.put("port", 0);
        fallbackInfo.put("ip", "0.0.0.0");
        return fallbackInfo;
    }
}
