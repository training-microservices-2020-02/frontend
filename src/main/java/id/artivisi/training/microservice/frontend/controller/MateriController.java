package id.artivisi.training.microservice.frontend.controller;

import id.artivisi.training.microservice.frontend.service.RegistrasiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MateriController {

    @Autowired private RegistrasiService registrasiService;

    @GetMapping("/materi/list")
    public ModelMap daftarMateriTraining() {
        return new ModelMap()
                .addAttribute("daftarMateri",
                        registrasiService.daftarMateri());
    }
}
