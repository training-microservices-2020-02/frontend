package id.artivisi.training.microservice.frontend.dto;

import lombok.Data;

@Data
public class MateriPelatihan {
    private String id;
    private String kode;
    private String nama;
    private Integer durasi;
}
